package org.task2;

public class Child extends Parent {
	
	public void property3() {
		System.out.println("property3");
	}
	
	public static void main(String[] args) {
		Child c=new Child();
		Parent parent=new Parent();
		Grandparent grandparent=new Grandparent();
		c.property1();
		c.property2();
		c.property3();
		parent.property1();
		parent.property2();
		grandparent.property1();
		
	}

}
