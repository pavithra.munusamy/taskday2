package org.task2;

public class Parent extends Grandparent {

	public void property2() {
		System.out.println("property2");
	}
	
	public static void main(String[] args) {
		Grandparent grandparent=new Grandparent();
		Parent parent=new Parent();
		grandparent.property1();
		parent.property1();
		parent.property2();
		
	}
	
}
