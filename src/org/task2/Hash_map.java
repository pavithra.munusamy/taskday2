package org.task2;

import java.util.HashMap;

public class Hash_map {
	
	public static void main(String[] args) {
		HashMap<Integer,String> a=new HashMap<Integer,String>();
		
		a.put(1, "Java");
		a.put(4, "Python");
		a.put(2, "Testing");
		a.put(3, "Selenium");
		System.out.println(a);
		
		int i=a.size();
		System.out.println(i);
		
		boolean b=a.containsKey(2);
		System.out.println(b);
	}

}
